<?xml version="1.0"?>
<!--
SPDX-FileCopyrightText: 2024 Lady <https://www.ladys.computer/about/#lady>
SPDX-License-Identifier: MPL-2.0

This Source Code Form is subject to the terms of the Mozilla Public License, v 2.0.
If a copy of the M·P·L was not distributed with this file, You can obtain one at <https://mozilla.org/MPL/2.0/>.
-->
<transform
	xmlns="http://www.w3.org/1999/XSL/Transform"
	xmlns:html="http://www.w3.org/1999/xhtml"
	xmlns:xslt="http://www.w3.org/1999/XSL/Transform"
	xmlns:书社="urn:fdc:ladys.computer:20231231:Shu1She4"
	version="1.0"
>
	<书社:id>urn:fdc:awoo.directory:20240501:metadata.xslt</书社:id>
	<template match="xslt:include[@书社:id='urn:fdc:awoo.directory:20240501:metadata.xslt']" mode="书社:metadata">
		<html:meta name="viewport" content="width=480"/>
	</template>
</transform>
