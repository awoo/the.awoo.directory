<?xml version="1.0"?>
<!--
SPDX-FileCopyrightText: 2024 Lady <https://www.ladys.computer/about/#lady>
SPDX-License-Identifier: MPL-2.0

This Source Code Form is subject to the terms of the Mozilla Public License, v 2.0.
If a copy of the M·P·L was not distributed with this file, You can obtain one at <https://mozilla.org/MPL/2.0/>.
-->
<transform
	xmlns="http://www.w3.org/1999/XSL/Transform"
	xmlns:Awoo="urn:fdc:awoo.directory:20240501:ns"
	xmlns:html="http://www.w3.org/1999/xhtml"
	xmlns:xslt="http://www.w3.org/1999/XSL/Transform"
	xmlns:书社="urn:fdc:ladys.computer:20231231:Shu1She4"
	exclude-result-prefixes="Awoo"
	version="1.0"
>
	<书社:id>urn:fdc:awoo.directory:20240501:header.xslt</书社:id>
	<template match="xslt:include[@书社:id='urn:fdc:awoo.directory:20240501:header.xslt']" mode="书社:header">
		<html:header>
			<html:a href="/">
				<html:strong>
					<html:span style="display:block;text-align:start">
						<text>the.</text>
					</html:span>
					<call-template name="Awoo:logo">
						<with-param name="styles" select="'display:block;margin:auto'"/>
					</call-template>
					<html:span style="display:block;text-align:end">
						<text>.directory</text>
					</html:span>
				</html:strong>
			</html:a>
		</html:header>
	</template>
</transform>
