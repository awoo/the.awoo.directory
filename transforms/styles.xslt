<?xml version="1.0"?>
<!--
SPDX-FileCopyrightText: 2024 Lady <https://www.ladys.computer/about/#lady>
SPDX-License-Identifier: MPL-2.0

This Source Code Form is subject to the terms of the Mozilla Public License, v 2.0.
If a copy of the M·P·L was not distributed with this file, You can obtain one at <https://mozilla.org/MPL/2.0/>.
-->
<transform
	xmlns="http://www.w3.org/1999/XSL/Transform"
	xmlns:html="http://www.w3.org/1999/xhtml"
	xmlns:xslt="http://www.w3.org/1999/XSL/Transform"
	xmlns:书社="urn:fdc:ladys.computer:20231231:Shu1She4"
	version="1.0"
>
	<书社:id>urn:fdc:awoo.directory:20240501:styles.xslt</书社:id>
	<template match="xslt:include[@书社:id='urn:fdc:awoo.directory:20240501:styles.xslt']" mode="书社:metadata">
		<html:style>
			<text>@charset "UTF-8";
@namespace "http://www.w3.org/1999/xhtml";
@namespace svg "http://www.w3.org/2000/svg";
html{ Margin: 0; Padding: 0; Color: #302F2B; Background: #FAF8F7; Font-Family: "Rockwell", "Rockwell Nova", "Roboto Slab", "DejaVu Serif", "Sitka Small", "Superclarendon", "Bookman Old Style", "URW Bookman", "Georgia Pro", "Georgia", "STIX Two Math", Serif; Font-Weight: 500; Line-Height: 1.5; Text-Align: Justify }
body{ Display: Flex; Box-Sizing: Border-Box; Margin: Auto; Padding: 1REM; Min-Block-Size: 100VH; Flex-Direction: Column }
body>header{ Padding: 1REM; Color: #DEDBD9; Background: Linear-Gradient(To Top Right, #302F2B 18CH, #57534C) Padding-Box, #FAF8F7 }
body>header>a[href="/"]{ Display: Block; Color: Inherit; Background: None; Inline-Size: Max-Content; Text-Decoration: None }
body>header>a[href="/"]:Active{ Background: None }
body>header>a[href="/"] svg|svg.AWOO{ Font-Size: 2EM }
body>header strong { Font-Size: 2REM; Font-Style: Inherit }
article{ Flex: Auto; Padding-Block: 1REM; Padding-Inline: 1CH; Color: #302F2B; Background: #FAF8F7 }
article>div.BODY,
article>section,
article>aside{ Margin: Auto; Border-Top: Medium Double; Padding: 1.5REM; Max-Inline-Size: 69REM }
article>div.BODY,
article>section>div.BODY{ Columns: 3 17REM; Column-Gap: 1.5REM }
article>aside>div.BODY{ Margin-Inline: Auto; Border: Medium #B8B3B0 Double; Padding: 3REM; Max-Inline-Size: 41REM; Box-Shadow: .25CH .25CH 1CH #57534C7F; Columns: Auto }
div.TABLE{ Column-Span: All; Margin-Block: 1.5EM; Overflow-X: Auto; Overflow-Y: Hidden }
table{ Margin-Block: 0; Margin-Inline: Auto; Border-Spacing: .25CH }
td,
th{ Padding-Block: 0; Padding-Inline: .5CH }
th{ Text-Decoration: Underline; Text-Decoration-Style: Wavy; Text-Decoration-Thickness: From-Font }
th:Nth-Of-Type(1){ Text-Decoration-Color: #FF6699 }
th:Nth-Of-Type(2){ Text-Decoration-Color: #663399 }
th:Nth-Of-Type(3){ Text-Decoration-Color: #FFCC33 }
th:Nth-Of-Type(4){ Text-Decoration-Color: #66CC33 }
td:Nth-Of-Type(1){ Background: #FF669947 }
td:Nth-Of-Type(2){ Background: #66339947 }
td:Nth-Of-Type(3){ Background: #FFCC3347 }
td:Nth-Of-Type(4){ Background: #66CC3347 }
td :Any-Link{ Margin-Block: Calc(-1PX - .125EM); Border: 1PX Solid; Padding-Block: .125REM; Padding-Inline: .5CH; -Webkit-Box-Decoration-Break: Clone; Box-Decoration-Break: Clone }
h1{ Margin-Block: 1.5REM; Font-Size: 3REM; Text-Align: Center }
h2{ Margin-Block: 0 2REM; Font-Size: 2REM }
aside h2{ Margin-Inline: Auto; Border-Bottom: Thin Solid; Padding-Inline: 1CH; Max-Inline-Size: Max-Content; Text-Align: Center }
h3{ Margin-Block: 0 1REM; Break-After: Avoid; Font-Size: Calc(2REM / 1.5) }
h3:Not(:First-Child){ Margin-Block-Start: 1.5REM }
ul{ Margin: 0; Padding: 0; List-Style-Type: "𝛌. " }
li{ Margin-Block: 1.5EM; Margin-Inline: 2EM }
p{ Margin: 0 }
p+p{ Text-Indent: 2EM }
p+p>*{ Text-Indent: 0 }
:Any-Link{ Margin-Block: -.125EM; Padding-Block: .125REM; Color: Inherit; Background: #FFCC337F }
:Any-Link:Active{ Background: #FF66997F }
code{ Font-Family: "Nimbus Mono PS", "Courier New", "Courier", Monospace; Font-Weight: Bold }
dfn{ Font-Style: Inherit; Font-Weight: Bold }
strong{ Font-Style: Italic; Font-Weight: Bold }
</text>
		</html:style>
	</template>
</transform>
