<?xml version="1.0"?>
<!--
SPDX-FileCopyrightText: 2024 Lady <https://www.ladys.computer/about/#lady>
SPDX-License-Identifier: MPL-2.0

This Source Code Form is subject to the terms of the Mozilla Public License, v 2.0.
If a copy of the M·P·L was not distributed with this file, You can obtain one at <https://mozilla.org/MPL/2.0/>.
-->
<transform
	xmlns="http://www.w3.org/1999/XSL/Transform"
	xmlns:Awoo="urn:fdc:awoo.directory:20240501:ns"
	xmlns:exslstr="http://exslt.org/strings"
	xmlns:html="http://www.w3.org/1999/xhtml"
	xmlns:svg="http://www.w3.org/2000/svg"
	xmlns:书社="urn:fdc:ladys.computer:20231231:Shu1She4"
	exclude-result-prefixes="Awoo"
	extension-element-prefixes="exslstr"
	version="1.0"
>
	<书社:id>urn:fdc:awoo.directory:20240501:logo.xslt</书社:id>
	<template name="Awoo:logo">
		<param name="id"/>
		<param name="class-list"/>
		<param name="styles"/>
		<svg:svg viewBox="0 0 184 82" stroke="currentColor" stroke-linejoin="round" stroke-linecap="round" stroke-width="2">
			<if test="string($id)!=''">
				<attribute name="id">
					<value-of select="$id"/>
				</attribute>
			</if>
			<attribute name="class">
				<text>AWOO</text>
				<if test="string($class-list)!=''">
					<for-each select="exslstr:tokenize($class-list)">
						<text> </text>
						<value-of select="."/>
					</for-each>
				</if>
			</attribute>
			<attribute name="style">
				<text>vertical-align:-.5ch;margin-top:-.5ch;height:auto;width:5ch</text>
				<if test="string($styles)!=''">
					<text>;</text>
					<value-of select="$styles"/>
				</if>
			</attribute>
			<svg:title>AWOO</svg:title>
			<svg:path fill="#FF6699" d="M 1 63 L 63 63 L 32 1 Z"/>
			<svg:path fill="#663399" transform="translate(42 0)" d="M 1 1 L 18 1 C 18 1 14 4 14 12 C 14 20 18 23 18 23 C 18 23 24 11 30 17 L 29 46 C 29 46 29 48 32 48 C 35 48 35 46 35 46 L 34 17 C 40 11 46 23 46 23 C 46 23 50 20 50 12 C 50 4 46 1 46 1 L 63 1 L 32 63 Z"/>
			<svg:circle fill="#FFCC33" cx="120" cy="40" r="22"/>
			<svg:line x1="120" x2="120" y1="63" y2="81"/>
			<svg:line x1="108" x2="132" y1="73" y2="73"/>
			<svg:circle fill="#66CC33" cx="161" cy="24" r="22"/>
			<svg:line x1="161" x2="161" y1="46" y2="63"/>
			<svg:line x1="149" x2="173" y1="55" y2="55"/>
		</svg:svg>
	</template>
	<template match="Awoo:logo">
		<call-template name="Awoo:logo">
			<with-param name="id" select="@id"/>
			<with-param name="class-list" select="@class"/>
			<with-param name="styles" select="@style"/>
		</call-template>
	</template>
	<template match="Awoo:sitename">
		<html:span>
			<if test="@id">
				<attribute name="id">
					<value-of select="@id"/>
				</attribute>
			</if>
			<attribute name="class">
				<text>SITENAME</text>
				<if test="string(@class)!=''">
					<for-each select="exslstr:tokenize(@class)">
						<text> </text>
						<value-of select="."/>
					</for-each>
				</if>
			</attribute>
			<attribute name="style">
				<text>white-space:pre</text>
				<if test="string(@style)!=''">
					<text>;</text>
					<value-of select="$styles"/>
				</if>
			</attribute>
			<text>the.</text>
			<call-template name="Awoo:logo"/>
			<text>.directory</text>
		</html:span>
	</template>
</transform>
