<?xml version="1.0"?>
<!--
SPDX-FileCopyrightText: 2024 Lady <https://www.ladys.computer/about/#lady>
SPDX-License-Identifier: MPL-2.0

This Source Code Form is subject to the terms of the Mozilla Public License, v 2.0.
If a copy of the M·P·L was not distributed with this file, You can obtain one at <https://mozilla.org/MPL/2.0/>.
-->
<transform
	xmlns="http://www.w3.org/1999/XSL/Transform"
	xmlns:html="http://www.w3.org/1999/xhtml"
	xmlns:书社="urn:fdc:ladys.computer:20231231:Shu1She4"
	version="1.0"
>
	<书社:id>urn:fdc:awoo.directory:20240501:article.xslt</书社:id>
	<template match="html:article|html:section[parent::html:article]|html:aside[parent::html:article]">
		<copy>
			<copy-of select="@*"/>
			<variable name="first" select="(text()[not(normalize-space()='')]|*[not(self::html:meta)])[1]"/>
			<variable name="firstsection" select="(html:section|html:aside)[1]"/>
			<choose>
				<when test="$first/self::html:*[translate(local-name(), '23456', '11111')='h1'][not(parent::html:aside)]">
					<apply-templates select="$first/preceding-sibling::node()|$first"/>
					<if test="$first/following-sibling::node()[not($firstsection) or following-sibling::node()[generate-id()=generate-id($firstsection)]]">
						<html:div class="BODY">
							<apply-templates select="$first/following-sibling::node()[not($firstsection) or following-sibling::node()[generate-id()=generate-id($firstsection)]]"/>
						</html:div>
					</if>
				</when>
				<when test="node()[not($firstsection) or following-sibling::node()[generate-id()=generate-id($firstsection)]]">
					<html:div class="BODY">
						<apply-templates select="node()[not($firstsection) or following-sibling::node()[generate-id()=generate-id($firstsection)]]"/>
					</html:div>
				</when>
			</choose>
			<apply-templates select="$firstsection|$firstsection/following-sibling::node()"/>
		</copy>
	</template>
</transform>
