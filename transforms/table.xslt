<?xml version="1.0"?>
<!--
SPDX-FileCopyrightText: 2024 Lady <https://www.ladys.computer/about/#lady>
SPDX-License-Identifier: MPL-2.0

This Source Code Form is subject to the terms of the Mozilla Public License, v 2.0.
If a copy of the M·P·L was not distributed with this file, You can obtain one at <https://mozilla.org/MPL/2.0/>.
-->
<transform
	xmlns="http://www.w3.org/1999/XSL/Transform"
	xmlns:html="http://www.w3.org/1999/xhtml"
	xmlns:书社="urn:fdc:ladys.computer:20231231:Shu1She4"
	version="1.0"
>
	<书社:id>urn:fdc:awoo.directory:20240501:table.xslt</书社:id>
	<template match="text()[normalize-space()='' and ancestor::书社:raw-text and (preceding-sibling::html:table[@书社:identifier='about:shushe?include=directory.tsv'] or following-sibling::html:table[@书社:identifier='about:shushe?include=directory.tsv'])]"/>
	<template match="html:table">
		<choose>
			<when test="ancestor::书社:raw-text and @书社:identifier='about:shushe?include=directory.tsv'">
				<text>[</text>
					<for-each select="html:tbody[1]/html:tr">
						<sort select="html:td[1]" data-type="number"/>
						<text>{&quot;id&quot;:</text>
						<value-of select="number(html:td[1])"/>
						<text>,&quot;name&quot;:&quot;</text>
						<value-of select="translate(html:td[2], '&#xA;&#xD;&quot;\', '  ＂＼')"/>
						<text>&quot;,&quot;website&quot;:&quot;</text>
						<value-of select="translate(html:td[3], '&#xA;&#xD;&quot;\', '  ＂＼')"/>
						<text>&quot;,&quot;owo&quot;:&quot;</text>
						<value-of select="translate(html:td[4], '&#xA;&#xD;&quot;\', '  ＂＼')"/>
						<text>&quot;}</text>
						<if test="following-sibling::html:tr">
							<text>,</text>
						</if>
					</for-each>
				<text>]</text>
			</when>
			<otherwise>
				<html:div class="TABLE">
					<copy>
						<apply-templates select="@*|node()"/>
					</copy>
				</html:div>
			</otherwise>
		</choose>
	</template>
	<template match="html:tbody[ancestor::html:table[@书社:identifier='about:shushe?include=directory.tsv']]">
		<copy>
			<apply-templates select="@*"/>
			<for-each select="node()">
				<sort select="self::html:tr/html:td[1]" data-type="number"/>
				<apply-templates select="."/>
			</for-each>
		</copy>
	</template>
	<template match="html:th[ancestor::html:table[@书社:identifier='about:shushe?include=directory.tsv']][not(preceding-sibling::*)]">
		<copy>
			<apply-templates select="@*[not(namespace-uri()='' and local-name()='style')]"/>
			<attribute name="style">
				<text>text-align:right</text>
				<if test="@style">
					<text>;</text>
					<value-of select="@style"/>
				</if>
			</attribute>
			<apply-templates/>
		</copy>
	</template>
	<template match="html:td[ancestor::html:table[@书社:identifier='about:shushe?include=directory.tsv']]">
		<copy>
			<apply-templates select="@*[not(namespace-uri()='' and local-name()='style')]"/>
			<choose>
				<when test="count(preceding-sibling::*)=0">
					<attribute name="style">
						<text>text-align:right</text>
						<if test="@style">
							<text>;</text>
							<value-of select="@style"/>
						</if>
					</attribute>
					<if test="number()&lt;10">
						<text>0</text>
					</if>
					<if test="number()&lt;100">
						<text>0</text>
					</if>
					<value-of select="number()"/>
				</when>
				<when test="count(preceding-sibling::*)=2">
					<apply-templates select="@style"/>
					<if test="string(.)!=''">
						<html:a href="{.}">
							<value-of select="."/>
						</html:a>
					</if>
				</when>
				<when test="count(preceding-sibling::*)=3">
					<attribute name="style">
						<text>text-align:center</text>
						<if test="@style">
							<text>;</text>
							<value-of select="@style"/>
						</if>
					</attribute>
					<apply-templates/>
				</when>
				<otherwise>
					<apply-templates select="@style"/>
					<apply-templates/>
				</otherwise>
			</choose>
		</copy>
	</template>
</transform>
