<!--
SPDX-FileCopyrightText: 2024 Lady <https://www.ladys.computer/about/#lady>
SPDX-License-Identifier: CC0-1.0
-->
# the.awoo.directory

Source code for <https://the.awoo.directory/>.
The directory itself is kept in [T·S·V](https://en.wikipedia.org/wiki/Tab-separated_values) format at `data/directory.tsv`.
The rest of the content files for the website are found in `sources/`.

Run `make` to build the site to `public/`.

## Dependencies

Building the website depends on [⛩️📰 书社](https://git.ladys.computer/Shushe), which is included as a Git submodule.

Assuming a Debian machine, ⛩️📰 书社 has the following dependencies :⁠—

- `libxml2-utils`
- `pax`
- `sharutils`
- `xsltproc`

The code *should* be portable, but may not be; email [Lady](https://www.ladys.computer/about/#lady) if you notice any bugs.

## The Awoo namespace

The Awoo namespace is `urn:fdc:awoo.directory:20240501:ns`.
With·in it, the following elements are defined :⁠—

- **`<Awoo:logo>`:**
  Transforms into the “AWOO” logotype (as an `<svg:svg>`).

- **`<Awoo:sitename>`:**
  Transforms into “the.AWOO.directory”, making use of the “AWOO” logotype.

## License, ⁊·c

This repository conforms to [Reuse](https://reuse.software).
As a general rule, content files are licensed Creative Commons Attribution‐ShareAlike 4.0 International, programmatic files are licensed under the Mozilla Public License, version 2.0, and trivial files are released into the public domain, but see the comment headers of each file to figure out which is which.
