# SPDX-FileCopyrightText: 2024 Lady <https://www.ladys.computer/about/#lady>
# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public License, v 2.0.
# If a copy of the M·P·L was not distributed with this file, You can obtain one at <https://mozilla.org/MPL/2.0/>.

SHELL = /bin/sh

SHUSHE := .⛩️📰
SHUSHEOPTS := DESTDIR='public' EXTRATRANSFORMS='$(wildcard transforms/*.xslt)' INCLUDEDIR='data'

build: $(SHUSHE)/GNUmakefile prebuild
	@for publicfile in $(patsubst %,"%",$(wildcard public/*)); do if git check-ignore -q $$publicfile; then rm -rf $$publicfile; fi; done
	@$(MAKE) -f $< install $(SHUSHEOPTS)

prebuild: $(SHUSHE)/GNUmakefile
	@$(MAKE) -f $< $(SHUSHEOPTS)

list: $(SHUSHE)/GNUmakefile
	@$(MAKE) -f $< $@ $(SHUSHEOPTS)

build/% public/%: $(SHUSHE)/GNUmakefile
	@$(MAKE) -f $< $@ $(SHUSHEOPTS)

$(SHUSHE)/GNUmakefile: %/GNUmakefile: FORCE
	git submodule update --init $*

gone:
	@for publicfile in $(patsubst %,"%",$(wildcard public/*)); do if git check-ignore -q $$publicfile; then rm -rf $$publicfile; fi; done

FORCE: ;

.PHONY: FORCE build gone list prebuild;
